import random from './random.js';

describe('random', () => {
	test('should return a number between 0 and 100', () => {
		const randomNumber = random(0, 100);

		expect(randomNumber >= 0 && randomNumber <= 100).toBe(true);
	})
})
