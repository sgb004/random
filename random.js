/**
 * @name random
 * @version 1.0.0
 * @author sgb004
 */

const random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

export default random;
