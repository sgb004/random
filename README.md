# Random

[![Version](https://img.shields.io/badge/version-1.0.1-blue.svg)](https://www.npmjs.com/package/sgb004-random)
[![Author](https://img.shields.io/badge/author-sgb004-green.svg)](https://sgb004.com)

This is a simple random number generator that allows you to generate a random number within a specified range.

## Installation

To install the `Random` you can use npm:

```node
npm i sgb004-random
```

## Usage

Import the `random` function into your project and use it to generate random numbers.

<!-- prettier-ignore -->
```js
import random from 'sgb004-random';

const min = 0;
const max = 100;
const randomNumber = random(min, max);

console.log(`Random number between ${min} and ${max}: ${randomNumber}`);
```

## Author

[sgb004](https://sgb004.com)

## License

MIT
