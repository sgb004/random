declare module 'random' {
  /**
   * Generates a random integer between the specified minimum (inclusive) and maximum (inclusive) values.
   * @param min - The minimum value for the random number (inclusive).
   * @param max - The maximum value for the random number (inclusive).
   * @returns A random integer between min and max (inclusive).
   */
  function random(min: number, max: number): number;

  export default random;
}
